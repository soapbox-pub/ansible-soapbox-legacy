# Soapbox Ansible

This repo contains configuration files for deploying a scalable Soapbox system with Ansible.
This is useful if you have to support hundreds or thousands of users on Soapbox, and you need a system that can scale.

The following playbooks are included:

* `provision.yml` - transforms servers into useful pieces of a bigger Soapbox system.
* `deploy.yml` - push local app config, deploy the latest Soapbox commit, and reload servers.

## Computer setup

### 1. Install Ansible

```sh
sudo add-apt-repository ppa:ansible/ansible
sudo apt install ansible
```

### 2. Clone this repo

```sh
git clone https://gitlab.com/soapbox-pub/ansible-soapbox.git
cd ansible-soapbox
```

### 3. Install Ansible dependencies

```sh
ansible-galaxy install -r requirements.yml
```


## Usage

### 1. Create servers on web host

First, you have to create the servers on your web host.
They will be transformed into one of the following types of server:

* Load balancer (`load`) x 1
* Sidekiq servers (`sidekiq`) x 2
* Puma servers (`web`) x 2
* Postgres/Redis server (`data`) x 1

You can start by creating **6 nodes** on your web host, using the smallest plan allowed (eg $5/mo).
Be sure to choose **Ubuntu 18.04 LTS** because this hasn't been tested on any other system.

### 2. Create inventory file

Create a new inventory file in the `data` directory.
For example, `data/mysite.ini`:

```
[load]
root@192.168.0.1 private_ip=10.0.0.1 letsencrypt_email=me@example.com

[web]
root@192.168.0.2 private_ip=10.0.0.2 puma=8 streaming=2
root@192.168.0.3 private_ip=10.0.0.3 puma=8 streaming=2

[sidekiq]
root@192.168.0.5 private_ip=10.0.0.5 default=2 pull=1 push=1
root@192.168.0.8 private_ip=10.0.0.8 default=1 pull=1 push=1 mailers=1

[data]
root@192.168.0.9 private_ip=10.0.0.9

[all:vars]
domain=example.com
```

Copy the above config and modify it so you use the public IP address of each node on the left, followed by the private IP (so the nodes can talk over the host's network).

The `web` and `sidekiq` groups need some extra configuration.

For `web` group:

* `puma` - number of Puma processes to run
* `streaming` - number of streaming processes to run

For `sidekiq` group:

* `default` - number of sidekiq "default" processes to run
* `push` - number of sidekiq "push" processes to run
* `pull` - number of sidekiq "pull" processes to run
* `mailers` - number of sidekiq "mailers" processes to run

You can add more nodes any time, just re-run the `provision.yml` playbook.

All servers must have a `private_ip` variable corresponding to their IP address on a private network.
This is used for configuring the firewall, nginx, and access rules.

### 3. Launching

#### 3.1 Provision

You'll provision all the servers initially.

> :warning: Note that this repo **assumes you are using Cloudflare**, and will route only Cloudflare traffic into the system.
You can disable this by commenting out the Cloudflare task in `provision.yml`, and running new commands on the load balancer's firewall to enable ports `80` and `443`.

You can re-run this command as many times as you want, and you should re-run it if your inventory changes.

```sh
# Run this the first time.
# Run it again if the inventory file changes.
# Run it again if you edit the configuration files in this repo.

ansible-playbook -i data/mysite.ini playbooks/provision.yml
```

#### 3.2 Configure

This is the only step that involves manual intervention.
You need to shell into your database server and fill out Soapbox's questionnaire.
You will only ever do this once.

```sh
# Shell into database server
ssh root@192.168.0.9

# Become soapbox user
su - soapbox
cd live

# Run setup wizard
RAILS_ENV=production /home/soapbox/.rbenv/shims/bundle exec rails gabsocial:setup
```
Once the configuration file gets written, you'll need to go back to your local computer and copy the file into the `data` folder of this repo.

```sh
scp root@192.168.0.9:/home/soapbox/live/.env.production data/example.com.ini
```

**Important:** The filename must match the `domain` variable from your inventory file.

Now you need to edit the configuration file, and ensure that you change the following lines:

```
DB_HOST=10.0.0.9
DB_PORT=6432
REDIS_HOST=10.0.0.9
```

You must set them to match the `private_ip` of the database node.
This file will be uploaded in the next step, and with every subsequent deploy.

#### 3.3 Deploy

Once you're ready to push, just run this command.

```sh
# Run to deploy the latest Soapbox commit

ansible-playbook -i data/mysite.ini playbooks/deploy.yml
```

At this point you should see a Soapbox site in your browser.
Congrats!
See [the Soapbox docs on some steps you can take next](https://docs.soapbox.pub/administration/post-installation/).
